<?php

use Taxibeat\Math\DistanceCalculator\Coordinate\DecimalCoordinate;

/**
 * Class CoordinateTest
 */
class CoordinateTest extends PHPUnit_Framework_TestCase
{
    public function testRadianLatitudeConversion()
    {
        $athens = new DecimalCoordinate(37.983166, 23.728957);

        $this->assertEquals(deg2rad($athens->getLatitude()), $athens->getRadianLatitude());
    }

    public function testRadianLongitudeConversion()
    {
        $athens = new DecimalCoordinate(37.983166, 23.728957);

        $this->assertEquals(deg2rad($athens->getLongitude()), $athens->getRadianLongitude());
    }
}