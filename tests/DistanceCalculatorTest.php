<?php

use Taxibeat\Math\DistanceCalculator\Coordinate\DecimalCoordinate;
use Taxibeat\Math\DistanceCalculator\DistanceCalculator;

/**
 * Class DistanceCalculatorTest
 */
class DistanceCalculatorTest extends PHPUnit_Framework_TestCase
{
    public function testDistanceBetweenAthensAndThiva()
    {
        //Athens
        $athens = new DecimalCoordinate(37.983166, 23.728957);

        //Thiva
        $thiva = new DecimalCoordinate(38.317661, 23.317973);

        $calculator = new DistanceCalculator();
        $distance = $calculator->getDistance($athens, $thiva);

        $this->assertEquals(51.72, $distance / 1000, '', 0.015);
    }
}