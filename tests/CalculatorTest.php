<?php

use Taxibeat\Math\Calculator\Calculator;

/**
 * Class CalculatorTest
 */
class CalculatorTest extends PHPUnit_Framework_TestCase
{
    public function testKmPerH2MperSec()
    {
        $this->assertEquals(Calculator::kmPerH2MperSec(100), 27.777777777778);
    }

    public function testCalcSpeed()
    {
        $this->assertEquals(Calculator::calcSpeed(
                5.3891290500935,
                1405594957,
                1405594966
            ),
            0.59879211667705
        );
    }

    public function testCalculateIdle()
    {
        $this->assertEquals(
            Calculator::calculateIdle(
                1405594966,
                1405594992,
                0.00051388888888889
            ),
            0.013361111111111
        );
    }

    public function testMoneyPerHour2MoneyPerSec()
    {
        $this->assertEquals(Calculator::moneyPerHour2MoneyPerSec(0.68), 0.00018888888888889);
    }

    public function testCalculateMoving()
    {
        $timeRanges = array(
            'nightTimeStart'    =>  strtotime('00:00:00'),
            'nightTimeStop'     =>  strtotime('05:00:00'),
            'dayTimeStar'       =>  strtotime('05:00:00'),
            'dayTimeStop'       =>  strtotime('00:00:00')
        );

        // Time (05:00, 00:00]
        $this->assertEquals(
            Calculator::calculateMoving(
                49.583915045891,
                1405594992, // 14:03:12
                0.00018888888888889,
                0.00033055555555556,
                $timeRanges
            ),
            0.0093658506197795
        );

        // Time (00:00, 05:00]
        $this->assertEquals(
            Calculator::calculateMoving(
                49.583915045891,
                1441579500, // 01:45:00
                0.00018888888888889,
                0.00033055555555556,
                $timeRanges
            ),
            0.016390238584614
        );
    }
}