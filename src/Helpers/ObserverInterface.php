<?php

namespace Taxibeat\Helpers;

/**
 * Interface ObserverInterface
 * @package Taxibeat\Helpers
 */
interface ObserverInterface
{
    /**
     * @param ObservableInterface $objSource
     * @param $message
     * @param $isInfoEvent
     */
    public function notify(ObservableInterface $objSource, $message, $isInfoEvent);
}