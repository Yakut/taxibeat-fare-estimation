<?php

namespace Taxibeat\Helpers;

/**
 * Interface ObservableInterface
 * @package Taxibeat\Helpers
 */
interface ObservableInterface
{
    /**
     * Adds the observer object into array of observers
     *
     * @param ObserverInterface $objObserver
     * @return void
     */
    public function addObserver(ObserverInterface $objObserver);

    /**
     * Sends event to all observers
     *
     * @param string $message
     * @param bool $isInfoEvent
     * @return void
     */
    public function sendEvent($message, $isInfoEvent);
}