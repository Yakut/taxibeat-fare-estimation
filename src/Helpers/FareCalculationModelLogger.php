<?php

namespace Taxibeat\Helpers;
use Taxibeat\Models\FareCalculationModel;

/**
 * Class FareCalculationModelLogger
 * @package Taxibeat\Helpers
 */
class FareCalculationModelLogger implements ObserverInterface
{
    /**
     * @inheritdoc
     */
    public function notify(ObservableInterface $objSource, $message, $isInfoEvent)
    {
        if($objSource instanceof FareCalculationModel && $isInfoEvent) {
            print("-> $message ");
        } else {
            print("$message \n");
        }
    }
}