<?php

namespace Taxibeat\Helpers;

/**
 * Class SQLiteDataIterator
 * @package Taxibeat\Helpers
 */
class SQLiteDataIterator implements \Iterator
{
    /**
     * @var \PDOStatement The PDO statement to execute
     */
    protected $pdoStatement;

    /**
     * @var int The cursor pointer
     */
    protected $key;

    /**
     * @var bool|array The Result for single row
     */
    protected $result;

    /**
     * @var bool The flag for validation of resource
     */
    protected $valid;

    public function __construct(\PDOStatement $statement)
    {
        $this->valid = true;
        $this->pdoStatement = $statement;
        $this->next();
    }

    /**
     * @inheritdoc
     */
    public function current()
    {
        return $this->result;
    }

    /**
     * @inheritdoc
     */
    public function next()
    {
        ++$this->key;
        $this->result = $this->pdoStatement->fetch(\PDO::FETCH_ASSOC);
        if (false === $this->result) {
            $this->valid = false;
            return null;
        }
    }
    /**
     * @inheritdoc
     */
    public function key()
    {
        return $this->key;
    }

    /**
     * @inheritdoc
     */
    public function valid()
    {
        return $this->valid;
    }

    /**
     * @inheritdoc
     */
    public function rewind()
    {
        $this->key = 0;
    }
}