<?php

namespace Taxibeat\Math\DistanceCalculator\Coordinate;

/**
 * Interface AbstractCoordinateInterface
 * @package Taxibeat\Math\DistanceCalculator\Coordinate
 */
interface AbstractCoordinateInterface
{
    /**
     * Returns the latitude coordinates
     *
     * @return float
     */
    public function getLatitude();

    /**
     * Sets the latitude coordinates
     *
     * @param float $latitude
     * @return float
     */
    public function setLatitude($latitude);

    /**
     * Returns the longitude coordinates
     *
     * @return float
     */
    public function getLongitude();

    /**
     * Sets the longitude coordinates
     *
     * @param float $longitude
     * @return mixed
     */
    public function setLongitude($longitude);

    /**
     * Returns the latitude coordinates in radian
     *
     * @return float
     */
    public function getRadianLatitude();

    /**
     * Returns the longitude coordinates in radian
     *
     * @return float
     */
    public function getRadianLongitude();

}