<?php

namespace Taxibeat\Math\DistanceCalculator\Coordinate;

/**
 * Class AbstractPoint
 * @package Taxibeat\Math\DistanceCalculator\Coordinate
 */
abstract class AbstractCoordinate implements AbstractCoordinateInterface
{
    /**
     * PI/180 constant
     */
    const DEGREE_TO_RADIAN = 0.017453292519943295769236907684886;

    /**
     * @var float
     */
    protected $latitude;

    /**
     * @var float
     */
    protected $longitude;

    /**
     * @inheritdoc
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @inheritdoc
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @inheritdoc
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @inheritdoc
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @inheritdoc
     */
    public function getRadianLatitude()
    {
        return self::DEGREE_TO_RADIAN * $this->latitude;
    }

    /**
     * @inheritdoc
     */
    public function getRadianLongitude()
    {
        return self::DEGREE_TO_RADIAN * $this->longitude;
    }
}