<?php

namespace Taxibeat\Math\DistanceCalculator\Coordinate;

/**
 * Class DecimalCoordinate
 * @package Taxibeat\Math\DistanceCalculator\Coordinate
 */
class DecimalCoordinate extends AbstractCoordinate
{
    /**
     * @param float $latitude
     * @param float $longitude
     */
    public function __construct($latitude = 0.0, $longitude = 0.0)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }
}