<?php

namespace Taxibeat\Math\DistanceCalculator;

use Taxibeat\Math\DistanceCalculator\Coordinate\AbstractCoordinateInterface;

/**
 * Class DistanceCalculator
 * @package Taxibeat\Math\DistanceCalculator
 */
class DistanceCalculator
{
    /**
     * @var float Earth radius in meters
     */
    private $earthRadius = 6372797.560856;

    /**
     * @return float
     */
    public function getEarthRadius()
    {
        return $this->earthRadius;
    }

    /**
     * @param float $earthRadius
     */
    public function setEarthRadius($earthRadius)
    {
        $this->earthRadius = $earthRadius;
    }

    /**
     * Computes the arc, in radian, between two WGS-84 positions.
     *
     * @param AbstractCoordinateInterface $positionFrom
     * @param AbstractCoordinateInterface $positionTo
     * @return float
     */
    private function arcInRadians(AbstractCoordinateInterface $positionFrom, AbstractCoordinateInterface $positionTo)
    {
        $latitudeArc = $positionFrom->getRadianLatitude() - $positionTo->getRadianLatitude();
        $longitudeArc = $positionFrom->getRadianLongitude() - $positionTo->getRadianLongitude();
        $latitudeH = pow(sin($latitudeArc * 0.5), 2);
        $longitudeH = pow(sin($longitudeArc * 0.5), 2);

        return 2.0 * asin(sqrt($latitudeH + cos($positionFrom->getRadianLatitude()) * cos($positionTo->getRadianLatitude()) * $longitudeH));
    }

    /**
     * Computes the distance, in meters, between two positions
     *
     * @param AbstractCoordinateInterface $positionFrom
     * @param AbstractCoordinateInterface $positionTo
     * @return float
     */
    public function getDistance(AbstractCoordinateInterface $positionFrom, AbstractCoordinateInterface $positionTo)
    {
        return $this->earthRadius * $this->arcInRadians($positionFrom, $positionTo);
    }
}