<?php

namespace Taxibeat\Math\Calculator;


/**
 * Class Calculator
 * @package Taxibeat\Math\Calculator
 */
class Calculator
{
    /**
     * Returns converted speed from km/h to m/s
     * @param $speed
     * @return float
     */
    public static function kmPerH2MperSec($speed)
    {
        return $speed / 3.6;
    }

    /**
     * Returns speed by distance and time points (m/s)
     *
     * @param $distance in m/s
     * @param $timePointA
     * @param $timePointB
     * @return float
     */
    public static function calcSpeed($distance, $timePointA, $timePointB)
    {
        if ($distance > 0)
        {
            $time = $timePointB - $timePointA;
            if ($time > 0) {
                return $distance / $time;
            }
        }

        return 0.0;
    }

    /**
     * Convert the fare in money per hour to money per second
     *
     * @param $money
     * @return float
     */
    public static function moneyPerHour2MoneyPerSec($money)
    {
        return $money / 3600.0;
    }

    /**
     * Convert money per km to money per m
     *
     * @param $money
     * @return float
     */
    public static function moneyPerKm2MoneyPerMeter($money)
    {
        return $money / 1000.0;
    }

    /**
     * Calculates idle fare
     *
     * @param $startIdleTime
     * @param $stopIdleTime
     * @param $idleFare
     * @return float money per sec
     */
    public static function calculateIdle($startIdleTime, $stopIdleTime, $idleFare)
    {
        return ($stopIdleTime - $startIdleTime) * $idleFare;
    }

    /**
     * Calculates fare estimation, when car was moved
     *
     * @param $distance
     * @param $rideTime
     * @param $dailyFare
     * @param $nightlyFare
     * @param array $timeRanges array(
            'nightTimeStart'    =>  timestamp,
            'nightTimeStop'     =>  timestamp,
            'dayTimeStar'       =>  timestamp,
            'dayTimeStop'       =>  timestamp
        );
     * @return float
     */
    public static function calculateMoving($distance, $rideTime, $dailyFare, $nightlyFare, array $timeRanges)
    {
        $rideTime = strtotime(date('H:i:s', $rideTime));
        if ($rideTime > $timeRanges['nightTimeStart'] && $rideTime <= $timeRanges['nightTimeStop']) {
            return $distance * $nightlyFare;
        } else { //if ($rideTime > $timeRanges['dayTimeStar'] && $rideTime <= $timeRanges['dayTimeStop']) {
            return $distance * $dailyFare;
        }

        return 0;
    }
}