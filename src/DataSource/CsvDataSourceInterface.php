<?php

namespace Taxibeat\DataSource;

/**
 * Interface CsvDataSourceInterface
 * @package Taxibeat\DataSource
 */
interface CsvDataSourceInterface
{
    /**
     * Sets file name for entity to work with
     *
     * @param string $fileName
     * @return void
     */
    public function setEntityFileName($fileName);

    /**
     * Fetches data rows from csv file and makes inserts to database table
     *
     * @return void
     */
    public function loadRowsIntoDb();

    /**
     * Exports the calculated data of fare estimate
     *
     * @return void
     */
    public function exportFareEstimateData();

}