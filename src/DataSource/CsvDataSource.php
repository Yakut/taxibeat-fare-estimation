<?php

namespace Taxibeat\DataSource;

use Taxibeat\Config\ConfigInterface;

/**
 * Class CsvDataSource
 * @package Taxibeat\DataSource
 */
class CsvDataSource implements CsvDataSourceInterface
{

    /**
     * @var DbRepositoryInterface object
     */
    private $db;

    /**
     * @var ConfigInterface object
     */
    private $config;

    /**
     * @var string
     */
    private $delimiter = ',';

    /**
     * @var string
     */
    private $inputFileName;

    public function __construct(DbRepositoryInterface $dbRepository, ConfigInterface $config)
    {
        $this->db = $dbRepository;
        $this->config = $config;
        $this->delimiter = $this->config->getDelimiter();
    }

    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function setEntityFileName($fileName)
    {
        if (!file_exists($this->config->getInputDataDir() . '/' . $fileName)) {
            throw new \Exception("Can't find file: {$fileName} in {$this->config->getInputDataDir()}");
        }

        $this->inputFileName = $fileName;
        $this->db->setTableName($this->inputFileName);
    }

    /**
     * @inheritdoc
     */
    public function loadRowsIntoDb()
    {
        $fileHandle = fopen($this->config->getInputDataDir() . '/' . $this->inputFileName, 'r');
        if ($fileHandle === false) {
            throw new \Exception("Opening file: {$this->inputFileName}  failed");
        }

        $this->db->startTransaction();
        try {
            $lineNumber = 1;
            while ($row = fgetcsv($fileHandle, 0, $this->delimiter)) {

                if (!isset($row[0]) || !strlen($row[0]) ||
                    !isset($row[1]) || !strlen($row[1]) ||
                    !isset($row[2]) || !strlen($row[2]) ||
                    !isset($row[3]) || !strlen($row[3])
                ) {

                    $this->db->rollBackTransaction();
                    throw new \Exception("Error in structure of file: {$this->inputFileName} line: {$lineNumber}");
                }

                $this->db->insertIntoTempTable($row[0], $row[1], $row[2], $row[3]);

                ++$lineNumber;
            }
            $this->db->commitTransaction();
        } catch (\PDOException $e) {
            $this->db->rollBackTransaction();
        }

        fclose($fileHandle);
    }

    /**
     * @inheritdoc
     */
    public function exportFareEstimateData()
    {
        $fileHandle = fopen($this->config->getOutputDataDir() . '/' . $this->inputFileName, 'w');
        if ($fileHandle === false) {
            throw new \Exception("Opening file: {$this->inputFileName}  failed");
        }

        $resultData = $this->db->getResultData();
        foreach($resultData as $row) {
            fputcsv($fileHandle, $row, $this->delimiter);
        }

        fclose($fileHandle);
    }
}