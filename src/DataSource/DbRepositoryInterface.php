<?php

namespace Taxibeat\DataSource;

/**
 * Interface DbRepositoryInterface
 * @package Taxibeat\DataSource
 */
interface DbRepositoryInterface
{

    /**
     * Sets the flag for deciding about table creation
     *
     * @param $value bool
     * @return void
     */
    public function setWillCreateNewTable($value);
    /**
     * Sets tmp table name to work with
     *
     * @param string $inputFileName
     * @return void
     */
    public function setTableName($inputFileName);

    /**
     * Returns table name
     *
     * @return string Table name
     */
    public function getTableName();
    /**
     * Initiates a transaction
     *
     * @return bool
     */
    public function startTransaction();

    /**
     * Commits a transaction
     *
     * @return bool
     */
    public function commitTransaction();

    /**
     * Rolls back a transaction
     *
     * @return bool
     */
    public function rollBackTransaction();

    /**
     * Inserts the row into temp table
     *
     * @param $idRide
     * @param $lat
     * @param $lng
     * @param $timestamp
     * @return bool
     */
    public function insertIntoTempTable($idRide, $lat, $lng, $timestamp);

    /**
     * Gets the Iterator for iterate of the ride identifiers
     *
     * @return  Iterator
     */
    public function getRideIds();

    /**
     * Returns the Iterator for iterate of ride information
     * by id ride for processing
     *
     * @param $idRide int ride identifier
     * @return Iterator
     */
    public function getRideDataByIdRide($idRide);

    /**
     * Deletes row from set of rides by id row
     *
     * @param int $idRow id row from set of rides
     * @return bool
     */
    public function delRideByIdRow($idRow);

    /**
     * Updates the speed and distance information in temp table
     *
     * @param $idRow
     * @param $speed
     * @param $distance
     * @return bool
     */
    public function updateSpeedAndDistance($idRow, $speed, $distance);

    /**
     * Inserts the row into result table
     *
     * @param $idRide
     * @param $fare
     * @return bool
     */
    public function insertIntoResultTable($idRide, $fare);

    /**
     * Gets the Iterator for iterate the calculated data of fare estimate
     *
     * @return Iterator
     */
    public function getResultData();
}