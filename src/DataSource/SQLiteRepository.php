<?php

namespace Taxibeat\DataSource;

use Taxibeat\Config\ConfigInterface;
use Taxibeat\Helpers\SQLiteDataIterator;

/**
 * Class SQLiteRepository
 * @package Taxibeat\DataSource
 */
class SQLiteRepository implements DbRepositoryInterface
{
    /**
     * @var \PDO
     */
    private $dbConnect;

    /**
     * @var string Name of tmp table, for calculation
     */
    private $tableName;

    /**
     * @var ConfigInterface object
     */
    private $config;

    /**
     * @var bool The flag for deciding about table creation
     */
    private $willCreateNewTable = true;

    /**
     * @var string Template for temp table creation
     */
    private $createTempTableSql =
        'CREATE TABLE #tableName#
            (
              id INTEGER PRIMARY KEY NOT NULL,
              id_ride INTEGER NOT NULL,
              lat REAL NOT NULL,
              lng REAL NOT NULL,
              timestamp TIMESTAMP NOT NULL,
              distance REAL,
              speed REAL
            );
            CREATE INDEX #tableName#_id_ride_idx ON #tableName#(id_ride ASC);
            CREATE INDEX #tableName#_timestamp_idx ON #tableName#(timestamp ASC)';

    /**
     * @var string Template for result temp table creation
     */
    private $createResultTableSql =
        'CREATE TABLE #tableName#_result
            (
              id INTEGER PRIMARY KEY NOT NULL,
              id_ride INTEGER NOT NULL,
              fare REAL NOT NULL
            );
            CREATE INDEX #tableName#_result_id_ride_idx ON #tableName#_result(id_ride ASC);';

    /**
     * @var string Template for inserting the information about fare of ride
     */
    private $insertIntoTmpResultTableSql =
        'INSERT INTO #tableName#_result(id_ride, fare) VALUES(:id_ride, :fare)';

    private $insertIntoTmpResultTableStatement;

    /**
     * @var string Template for inserting the rides
     */
    private $insertIntoTmpTableSql =
        'INSERT INTO #tableName#(id_ride, lat, lng, timestamp) VALUES (:id_ride, :lat, :lng, :timestamp)';

    /**
     * @var \PDOStatement Statement for inserting data into temp table
     */
    private $insertIntoTmpTableStatement;

    /**
     * @var string Template for updating speed and  distance
     */
    private $updateSpeedAndDistanceSql =
        'UPDATE #tableName# SET speed = :speed, distance = :distance WHERE id = :id';

    /**
     * @var \PDOStatement Statement for updating  speed and distance information in temp table
     */
    private $updateSpeedAndDistanceStatement;

    /**
     * @var string Template for deleting the ride by id
     */
    private $deleteRideByIdSql =
        'DELETE FROM #tableName# WHERE id = :id';


    /**
     * @var \PDOStatement Statement for deleting the ride by id
     */
    private $deleteRideByIdStatement;

    /**
     * @var string Template for selecting the ride IDs
     */
    private $getRidIdsSql =
        'SELECT id_ride FROM #tableName# GROUP BY id_ride';

    /**
     * @var \PDOStatement Statement for selecting the ride IDs
     */
    private $getRidIdsStatement;

    /**
     * @var string Template for iterate of ride information by id ride
     */
    private $getRideDataByIdRideSql =
        'SELECT id, lat, lng, timestamp, distance, speed FROM #tableName# WHERE id_ride = :idride';

    /**
     * @var \PDOStatement Statement for selecting the ride information by id ride
     */
    private $getRideDataByIdRideStatement;

    /**
     * @var string Template for selecting the calculated data of fare estimate
     */
    private $getResultDataSql =
        'SELECT id_ride, fare FROM #tableName#_result';

    /**
     * @var \PDOStatement Statement for selecting the calculated data of fare estimate
     */
    private $getResultDataStatement;


    public function __construct(ConfigInterface $config)
    {
        $this->config = $config;

        try {
            // Create (connect to) SQLite database in file
            $this->dbConnect = new \PDO($this->config->getDsn());
            $this->dbConnect->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            die("SQLiteRepository error: {$e->getMessage()} \n");
        }
    }

    /**
     * Creates temp table and return new temp table name
     *
     * @param $fileName
     * @return string New table name
     */
    private function createTempTable($fileName)
    {
        try {
            $this->dbConnect->exec(str_replace('#tableName#', $fileName, $this->createTempTableSql));

            return $fileName;
        } catch (\PDOException $e) {
            die("SQLiteRepository error: {$e->getMessage()} \n");
        }
    }

    /**
     * Creates temp result table and return new temp result table name
     *
     * @param $fileName
     * @return void
     */
    private function createTempResultTable($fileName)
    {
        try {
            $this->dbConnect->exec(str_replace('#tableName#', $fileName, $this->createResultTableSql));

        } catch (\PDOException $e) {
            die("SQLiteRepository error: {$e->getMessage()} \n");
        }
    }

    /**
     * Generate new file name and creates temp tables
     *
     * @param $fileName
     * @return string New table name
     */
    private function createTempTableName($fileName)
    {
        try {
            if ($this->dbConnect->query("SELECT 1 FROM {$fileName}")) {

                if (!$this->willCreateNewTable) {
                    // Use created table
                    return $fileName;
                }

                $newFileName = $fileName . '_' . date('d_m_Y_H_i_s');
                $this->createTempResultTable($newFileName);

                return $this->createTempTable($newFileName);
            }
        } catch (\PDOException $e) {
            $this->createTempResultTable($fileName);

            return $this->createTempTable($fileName);
        }
    }

    /**
     * Prepare statements
     *
     * @return void
     */
    private function prepareStatements()
    {
        $this->insertIntoTmpTableStatement =
            $this->dbConnect->prepare(str_replace('#tableName#', $this->tableName, $this->insertIntoTmpTableSql));

        $this->updateSpeedAndDistanceStatement =
            $this->dbConnect->prepare(str_replace('#tableName#', $this->tableName, $this->updateSpeedAndDistanceSql));

        $this->deleteRideByIdStatement =
            $this->dbConnect->prepare(str_replace('#tableName#', $this->tableName, $this->deleteRideByIdSql));

        $this->getRidIdsStatement =
            $this->dbConnect->prepare(str_replace('#tableName#', $this->tableName, $this->getRidIdsSql));

        $this->getRideDataByIdRideStatement =
            $this->dbConnect->prepare(str_replace('#tableName#', $this->tableName, $this->getRideDataByIdRideSql));

        $this->insertIntoTmpResultTableStatement =
            $this->dbConnect->prepare(str_replace('#tableName#', $this->tableName, $this->insertIntoTmpResultTableSql));

        $this->getResultDataStatement =
            $this->dbConnect->prepare(str_replace('#tableName#', $this->tableName, $this->getResultDataSql));
    }

    /**
     * @inheritdoc
     */
    public function setWillCreateNewTable($value)
    {
        $this->willCreateNewTable = $value;
    }

    /**
     * @inheritdoc
     */
    public function setTableName($fileName)
    {
        $this->tableName = $this->createTempTableName(str_replace('.', '_', $fileName));

        $this->prepareStatements();
    }

    /**
     * @inheritdoc
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @inheritdoc
     */
    public function startTransaction()
    {
        return $this->dbConnect->beginTransaction();
    }

    /**
     * @inheritdoc
     */
    public function commitTransaction()
    {
        return $this->dbConnect->commit();
    }

    /**
     * @inheritdoc
     */
    public function rollBackTransaction()
    {
        return $this->dbConnect->rollBack();
    }

    /**
     * @inheritdoc
     */
    public function insertIntoTempTable($idRide, $lat, $lng, $timestamp)
    {
        return $this->insertIntoTmpTableStatement->execute(array(
            ':id_ride' => $idRide,
            ':lat' => $lat,
            ':lng' => $lng,
            ':timestamp' => $timestamp
        ));
    }

    /**
     * @inheritdoc
     *
     * @return SQLiteDataIterator
     */
    public function getRideIds()
    {
        try {

            $this->getRidIdsStatement->execute();

            return new SQLiteDataIterator($this->getRidIdsStatement);
        } catch (\PDOException $e) {
            die("SQLiteRepository error: {$e->getMessage()} \n");
        }
    }

    /**
     * @inheritdoc
     *
     * @return SQLiteDataIterator
     */
    public function getRideDataByIdRide($idRide)
    {
        try {

            $this->getRideDataByIdRideStatement->execute(array(
                ':idride' => $idRide
            ));

            return new SQLiteDataIterator($this->getRideDataByIdRideStatement);
        } catch (\PDOException $e) {
            die("SQLiteRepository error: {$e->getMessage()} \n");
        }
    }

    /**
     * @inheritdoc
     */
    public function delRideByIdRow($idRow)
    {
        try {
            return $this->deleteRideByIdStatement->execute(array(
                ':id' => $idRow
            ));
        } catch (\PDOException $e) {
            die("SQLiteRepository error: {$e->getMessage()} \n");
        }
    }

    /**
     * @inheritdoc
     */
    public function updateSpeedAndDistance($idRow, $speed, $distance)
    {
        try {
            return $this->updateSpeedAndDistanceStatement->execute(array(
                ':id' => $idRow,
                ':speed' => $speed,
                ':distance' => $distance
            ));
        } catch (\PDOException $e) {
            die("SQLiteRepository error: {$e->getMessage()} \n");
        }
    }

    /**
     * @inheritdoc
     */
    public function insertIntoResultTable($idRide, $fare)
    {
        try {
            return $this->insertIntoTmpResultTableStatement->execute(array(
                ':id_ride' => $idRide,
                ':fare' => $fare
            ));
        } catch (\PDOException $e) {
            die("SQLiteRepository error: {$e->getMessage()} \n");
        }
    }

    /**
     * @inheritdoc
     *
     * @return SQLiteDataIterator
     */
    public function getResultData()
    {
        try {

            $this->getResultDataStatement->execute();

            return new SQLiteDataIterator($this->getResultDataStatement);
        } catch (\PDOException $e) {
            die("SQLiteRepository error: {$e->getMessage()} \n");
        }
    }
}