<?php

namespace Taxibeat\Models;

use Taxibeat\Config\ConfigInterface;
use Taxibeat\DataSource\CsvDataSource;
use Taxibeat\DataSource\SQLiteRepository;
use Taxibeat\Helpers\ObservableInterface;
use Taxibeat\Helpers\ObserverInterface;
use Taxibeat\Math\Calculator\Calculator;
use Taxibeat\Math\DistanceCalculator\Coordinate\DecimalCoordinate;
use Taxibeat\Math\DistanceCalculator\DistanceCalculator;

/**
 * Class FareCalculationModel
 * @package Taxibeat\Models
 */
class FareCalculationModel implements ObservableInterface
{
    /**
     * @var string Input scv file name
     */
    private $inputFileName;

    /**
     * @var CsvDataSourceInterface
     */
    private $csvDataSource;

    /**
     * @var DbRepositoryInterface
     */
    private $db;

    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * @var array
     */
    protected $observersArray = array();

    public function __construct($inputFileName, ConfigInterface $config)
    {
        $this->inputFileName = $inputFileName;
        $this->config = $config;

        switch ($this->config->getDbDriverName()) {
            case 'SQLite':
                $this->db = new SQLiteRepository($this->config);
                break;
        }

        $this->csvDataSource = new CsvDataSource($this->db, $this->config);
    }

    /**
     * Loads data from csv file to database
     *
     * @throws \Exception
     */
    public function loadData()
    {
        $this->sendEvent('Inserting data from csv file into database..', true);
        $this->db->setWillCreateNewTable(false);
        $this->csvDataSource->setEntityFileName($this->inputFileName);
        $this->csvDataSource->loadRowsIntoDb();
        $this->sendEvent('OK');
    }

    /**
     * Runs the process of data filtering
     */
    public function runDataFiltering()
    {
        $this->sendEvent('Data filtering ..', true);
        $this->dataFiltering();
        $this->sendEvent('OK');
    }

    /**
     * Runs the process of fare estimation
     */
    public function runFareEstimation()
    {
        $this->sendEvent('Estimation of Fare ..', true);
        $this->fareEstimation();
        $this->sendEvent('OK');
    }

    public function runExportFareEstimateData()
    {
        $this->sendEvent('Exporting the calculated data of fare estimate ..', true);
        $this->csvDataSource->exportFareEstimateData();
        $this->sendEvent('OK');
    }

    /**
     * Data filtering step
     * A filter pass should be performed on the input tuples to get rid of invalid
     * entries. The filter should process segments of consecutive tuples p1, p2 and
     * calculate the segment's speed U. If the speed exceeds 100 km/h then tuple p2
     * should be removed from the set.
     *
     * @return void
     */
    private function dataFiltering()
    {
        // Convert km/h to m/s
        $maxSpeed = Calculator::kmPerH2MperSec($this->config->getMaxSpeed());
        // Prepare objects for calculation
        $distanceCalculator = new DistanceCalculator();
        $positionA = new DecimalCoordinate();
        $positionB = new DecimalCoordinate();

        $rideIds = $this->db->getRideIds();
        // Processing of ride identifiers
        $this->sendEvent("");
        foreach ($rideIds as $ride) {
            $idRide = $ride['id_ride'];
            $this->sendEvent("  ->Processing ride id # $idRide");
            // Processing data filtering
            $firstPoint = null;
            // Gets the ride data by ride id
            $rideDataByIdRide = $this->db->getRideDataByIdRide($idRide);
            foreach ($rideDataByIdRide as $point) {
                if (!isset($firstPoint)) {
                    $firstPoint = $point;
                } else {
                    $positionA->setLatitude($firstPoint['lat']);
                    $positionA->setLongitude($firstPoint['lng']);

                    $positionB->setLatitude($point['lat']);
                    $positionB->setLongitude($point['lng']);

                    // distance in meters
                    $distance = $distanceCalculator->getDistance($positionA, $positionB);
                    // speed in m/s
                    $speed = Calculator::calcSpeed($distance, $firstPoint['timestamp'], $point['timestamp']);

                    if ($speed > $maxSpeed) {
                        // delete failed point
                        $this->db->delRideByIdRow($point['id']);
                    } else {
                        //update point speed and distance data
                        $this->db->updateSpeedAndDistance($point['id'], $speed, $distance);
                        $firstPoint = $point;
                    }
                }
            }
        }
    }

    /**
     * Fare estimation step
     * The total fare estimate for the ride is calculated by aggregating the individual
     * estimates for the ride segments using the following rules:
     * __________________________________________________________________________
     * State            | Applied                 | Fare
     * __________________________________________________________________________
     *   MOVING         | Time (05:00, 00:00]     |  0.68 per km
     *   (U > 10 km/h)  | Time (00:00, 05:00]     |  1.19 per km
     * __________________________________________________________________________
     *   IDLE           | ALWAYS                  |  1.85 per hour of idle time
     *   (U <= 10 km/h) |                         |
     * __________________________________________________________________________
     *
     *  At the start of each ride, a standard “flag” amount of 1.19 is added to the
     *  total fare. The ride total fare should be at least 3.16.
     *
     * @return void
     */
    private function fareEstimation()
    {
        // m/s
        $movingSpeed = Calculator::kmPerH2MperSec($this->config->getMoving());

        // money per sec
        $dailyFare = Calculator::moneyPerHour2MoneyPerSec($this->config->getDailyFare());
        $nightlyFare = Calculator::moneyPerHour2MoneyPerSec($this->config->getNightlyFare());

        $totalFareLimit = $this->config->getTotalFareLimit();

        // timestamp, start of idle time
        $startIdleTime = 0;
        // money per sec
        $idleFare = Calculator::moneyPerHour2MoneyPerSec($this->config->getIdleFare());
        //
        $timeRanges = array(
            'nightTimeStart'    =>  $this->config->getNightTimeStart(),
            'nightTimeStop'     =>  $this->config->getNightTimeStop(),
            'dayTimeStar'       =>  $this->config->getDayTimeStart(),
            'dayTimeStop'       =>  $this->config->getDayTimeStop()
        );

        $rideIds = $this->db->getRideIds();
        // Processing of ride identifiers
        $this->sendEvent("");
        foreach ($rideIds as $ride) {
            $idRide = $ride['id_ride'];
            $this->sendEvent("  ->Processing ride id # $idRide");

            $firstPoint = false;

            // Start of each ride, a standard “flag” amount
            $fare = $this->config->getStandardFlag();
            $startIdleTime = 0;
            // Gets the ride data by ride id
            $rideDataByIdRide = $this->db->getRideDataByIdRide($idRide);
            foreach ($rideDataByIdRide as $point) {
                if (!$firstPoint) {
                    $firstPoint = true;
                } else {
                    // moving
                    if ($point['speed'] > $movingSpeed) {
                        // Idle was stop. Calculate idle fare
                        if ($startIdleTime > 0) {
                            $fare += Calculator::calculateIdle(
                                $startIdleTime,
                                $point['timestamp'],
                                $idleFare
                            );
                            $startIdleTime = 0;
                        }

                        $fare += Calculator::calculateMoving(
                            $point['distance'],
                            $point['timestamp'],
                            $dailyFare,
                            $nightlyFare,
                            $timeRanges
                        );

                    } elseif (!$startIdleTime) {
                        // store start idle
                        $startIdleTime = $point['timestamp'];
                    }
                }
            }

            // The total fare of ride should be at least
            if ($fare < $totalFareLimit) {
                $fare = $totalFareLimit;
            }

            $this->db->insertIntoResultTable($idRide, $fare);
        }
    }

    /**
     * @inheritdoc
     */
    public function addObserver(ObserverInterface $objObserver)
    {
        if (!in_array($objObserver, $this->observersArray)) {
            $this->observersArray[] = $objObserver;
        }
    }

    /**
     * @inheritdoc
     */
    public function sendEvent($message, $isInfoEvent = false)
    {
        foreach ($this->observersArray as $objObserver) {
            $objObserver->notify($this, $message, $isInfoEvent);
        }
    }
}