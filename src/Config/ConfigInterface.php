<?php

namespace Taxibeat\Config;

/**
 * Interface ConfigInterface
 * @package Taxibeat\Config
 */
interface ConfigInterface
{
    /**
     * Returns name of database driver
     *
     * @return string Name of driver
     */
    public function getDbDriverName();

    /**
     * Returns data source name
     *
     * @return string Data source name
     */
    public function getDsn();

    /**
     * Returns user name for database connect
     *
     * @return string User name
     */
    public function getUserName();

    /**
     * Returns password string for database connect
     *
     * @return string password
     */
    public function getUerPassword();

    /**
     * Returns path to directory with input data
     *
     * @return string Path to directory with csv data files
     */
    public function getInputDataDir();

    /**
     * Returns path to directory with output data
     *
     * @return string Path to directory with csv data files
     */
    public function getOutputDataDir();

    /**
     * Returns number of rows in page,
     * for db operations
     *
     * @return int Number of rows
     */
    public function getNumberRowsInPage();

    /**
     * Returns max speed for data filtering
     *
     * @return int max speed in km/h
     */
    public function getMaxSpeed();

    /**
     * Returns delimiter char
     *
     * @return string Delimiter char
     */
    public function getDelimiter();

    /**
     * Returns total fare of rid
     *
     * @return float
     */
    public function getTotalFareLimit();

    /**
     * Returns standard “flag” amount
     *
     * @return float
     */
    public function getStandardFlag();

    /**
     * Returns speed that shows of moving the car
     *
     * @return float
     */
    public function getMoving();

    /**
     * Returns start of nighttime estimation
     *
     * @return int
     */
    public function getNightTimeStart();

    /**
     * Returns end of nighttime estimation
     *
     * @return int
     */
    public function getNightTimeStop();

    /**
     * Returns nightly fare
     *
     * @return float
     */
    public function getNightlyFare();

    /**
     * Returns start of daytime estimation
     *
     * @return int
     */
    public function getDayTimeStart();

    /**
     * Returns end of daytime estimation
     *
     * @return int
     */
    public function getDayTimeStop();

    /**
     * Returns daily fare
     *
     * @return float
     */
    public function getDailyFare();

    /**
     * Returns idle  fare
     *
     * @return float
     */
    public function getIdleFare();
}