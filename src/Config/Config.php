<?php

namespace Taxibeat\Config;

/**
 * Class Config
 * @package Taxibeat\Config
 */
class Config implements ConfigInterface
{
    /**
     * @var string Path to ini file
     */
    private $pathToIniFile;

    /**
     * @var string Name of database driver
     */
    private $driverName;

    /**
     * @var string Data source name
     */
    private $dsn;

    /**
     * @var string User name for database connect
     */
    private $userName = '';

    /**
     * @var string Password string for database connect
     */
    private $password = '';

    /**
     * @var int Max speed for data filtering
     */
    private $maxSpeed = 100;

    /**
     * @var integer Number of rows
     */
    private $rowsInPage = 100;

    /**
     * @var string Csv file data delimiter
     */
    private $delimiter = ',';

    /**
     * @var string Path to directory with input data
     */
    private $inputDataDirPath;

    /**
     * @var string Path to directory with output data
     */
    private $outputDataDirPath;

    /**
     * @var float The total fare of ride should be at least
     */
    private $totalFareLimit;

    /**
     * @var float At the start of each ride, a standard “flag” amount should be added to the total fare
     */
    private $standard;

    /**
     * @var float Speed that shows of moving the car
     */
    private $moving;

    /**
     * @var int Start of nighttime estimation
     */
    private $nightTimeStart;

    /**
     * @var int End of nighttime estimation
     */
    private $nightTimeStop;

    /**
     * @var float Nightly fare
     */
    private $nightlyFare;

    /**
     * @var int Start of daytime estimation
     */
    private $dayTimeStart;

    /**
     * @var int End of daytime estimation
     */
    private $dayTimeStop;

    /**
     * @var float Daily fare
     */
    private $dailyFare;

    /**
     * @var float Idle fare (per hour of idle time)
     */
    private $idleFare;

    public function __construct($pathToIniFile = null)
    {
        if (!isset($pathToIniFile)) {
            $this->pathToIniFile = __DIR__ . '/../../config/config.ini';
        } else {
            $this->pathToIniFile = $pathToIniFile;
        }

        $this->init();
    }

    /**
     * Getting the config data from ini file
     *
     * @throws \Exception
     */
    private function init()
    {
        $iniArray = parse_ini_file($this->pathToIniFile);

        if (!isset($iniArray['driver'])) {
            throw new \Exception('Database driver was not set in config file.');
        }
        $this->driverName = $iniArray['driver'];

        if (!isset($iniArray['dsn'])) {
            throw new \Exception('Database data source name was not set in config file.');
        }
        $this->dsn = $iniArray['dsn'];

        if (isset($iniArray['username'])) {
            $this->userName = $iniArray['username'];
        }

        if (isset($iniArray['password'])) {
            $this->password = $iniArray['password'];
        }

        if (isset($iniArray['maxspeed'])) {
            $this->maxSpeed = $iniArray['maxspeed'];
        }

        if (isset($iniArray['rowsinpage'])) {
            $this->rowsInPage = $iniArray['rowsinpage'];
        }

        if (isset($iniArray['delimiter'])) {
            $this->delimiter = $iniArray['delimiter'];
        }

        if (!isset($iniArray['input'])) {
            throw new \Exception('Path to directory with input data was not set in config file.');
        }
        $this->inputDataDirPath = $iniArray['input'];

        if (!isset($iniArray['output'])) {
            throw new \Exception('Path to directory with output data was not set in config file.');
        }
        $this->outputDataDirPath = $iniArray['output'];

        if (!isset($iniArray['total_fare_limit'])) {
            throw new \Exception('Total fare was not set in config file.');
        }
        $this->totalFareLimit = $iniArray['total_fare_limit'];

        if (!isset($iniArray['standard'])) {
            throw new \Exception('Standard “flag” amount was not set in config file.');
        }
        $this->standard = $iniArray['standard'];

        if (!isset($iniArray['moving'])) {
            throw new \Exception('Speed that shows of moving the car was not set in config file.');
        }
        $this->moving = $iniArray['moving'];

        if (!isset($iniArray['nighttime_start'])) {
            throw new \Exception('Start of nighttime estimation was not set in config file.');
        }
        $this->nightTimeStart = strtotime($iniArray['nighttime_start']);

        if (!isset($iniArray['nighttime_stop'])) {
            throw new \Exception('End of nighttime estimation was not set in config file.');
        }
        $this->nightTimeStop = strtotime($iniArray['nighttime_stop']);

        if (!isset($iniArray['night_fare'])) {
            throw new \Exception('Nightly fare was not set in config file.');
        }
        $this->nightlyFare = $iniArray['night_fare'];

        if (!isset($iniArray['daytime_start'])) {
            throw new \Exception('Start of daytime estimation was not set in config file.');
        }
        $this->dayTimeStart = strtotime($iniArray['daytime_start']);

        if (!isset($iniArray['daytime_stop'])) {
            throw new \Exception('End of daytime estimation was not set in config file.');
        }
        $this->dayTimeStop = strtotime($iniArray['daytime_stop']);

        if (!isset($iniArray['daily_fare'])) {
            throw new \Exception('Daily fare was not set in config file.');
        }
        $this->dailyFare = $iniArray['daily_fare'];

        if (!isset($iniArray['idle_fare'])) {
            throw new \Exception('Idle fare was not set in config file.');
        }
        $this->idleFare = $iniArray['idle_fare'];
    }

    /**
     * @inheritdoc
     */
    public function getDbDriverName()
    {
        return $this->driverName;
    }

    /**
     * @inheritdoc
     */
    public function getDsn()
    {
        return $this->dsn;
    }

    public function getUserName()
    {
        $this->userName;
    }

    /**
     * @inheritdoc
     */
    public function getUerPassword()
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function getInputDataDir()
    {
        return $this->inputDataDirPath;
    }

    /**
     * @inheritdoc
     */
    public function getOutputDataDir()
    {
        return $this->outputDataDirPath;
    }

    /**
     * @inheritdoc
     */
    public function getNumberRowsInPage()
    {
        return $this->rowsInPage;
    }

    /**
     * @inheritdoc
     */
    public function getMaxSpeed()
    {
        return $this->maxSpeed;
    }

    /**
     * @inheritdoc
     */
    public function getDelimiter()
    {
        return $this->delimiter;
    }

    /**
     * @inheritdoc
     */
    public function getTotalFareLimit()
    {
        return $this->totalFareLimit;
    }

    /**
     * @inheritdoc
     */
    public function getStandardFlag()
    {
        return $this->standard;
    }

    /**
     * @inheritdoc
     */
    public function getMoving()
    {
        return $this->moving;
    }

    /**
     * @inheritdoc
     */
    public function getNightTimeStart()
    {
        return $this->nightTimeStart;
    }

    /**
     * @inheritdoc
     */
    public function getNightTimeStop()
    {
        return $this->nightTimeStop;
    }

    /**
     * @inheritdoc
     */
    public function getNightlyFare()
    {
        return $this->nightlyFare;
    }

    /**
     * @inheritdoc
     */
    public function getDayTimeStart()
    {
        return $this->dayTimeStart;
    }

    /**
     * @inheritdoc
     */
    public function getDayTimeStop()
    {
        return $this->dayTimeStop;
    }

    /**
     * @inheritdoc
     */
    public function getDailyFare()
    {
        return $this->dailyFare;
    }

    /**
     * @inheritdoc
     */
    public function getIdleFare()
    {
        return $this->idleFare;
    }
}