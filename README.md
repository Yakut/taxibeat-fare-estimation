# Taxibeat test task

### Setting up

1. Install composer [https://getcomposer.org/download/]
2. Run `composer install` command to install dependencies and generate autoload file

### Usage

You can run command `php calculate.php <input file name>` to get output file with calculated fare estimate

 - `input file name` - located in /data/input directory
 - `output file name` - located in /data/output directory

### Description of application

#### Project structure:

 - `config` - The directory contains a configuration file
 - `data` - The directory contains subdirectory `input` (input data files, *.csv ), `output`  (output data files, with calculated fare estimate *.csv )
 - `database` - The directory contains a database file (for SQLite driver)
 - `src` - Source files of the application
 - `tests` - Source files of Unit tests

#### Source Overview:
##### _Taxibeat\Models\FareCalculationModel_: 
It is model which contains such of functionality steps:

- data loading;
- data filtering;
- fare estimate calculation;
- exports of calculated data to output csv file;

##### _Taxibeat\Config\Config_: 
It is a class for serialize of config file

##### _Taxibeat\DataSource\CsvDataSource_: 
It is a class for csv data loading into database and
exporting of fare estimate data into output csv file

##### _Taxibeat\DataSource\SQLiteRepository_: 
It is a class for database manipulation using SQLite driver
You can develop repository class implementing DbRepositoryInterface 
for another DB driver, like MYSQL 

##### _Taxibeat\Helpers\FareCalculationModelLogger_: 
It is an observer class for logging events of FareCalculationModel class
 
##### _Taxibeat\Helpers\SQLiteDataIterator_: 
It is a class for iteration of data selected from database

##### _Taxibeat\Math\Calculator_: 
It is a class for different types of math calculation and conversion

##### _Taxibeat\Math\DistanceCalculator_: 
It is a class for computing the distance, in meters, between two positions

##### _Taxibeat\calculate.php_: 
It is a main script that shows how to use application