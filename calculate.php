<?php
require __DIR__ . '/vendor/autoload.php';
mb_internal_encoding('UTF-8');

use Taxibeat\Models\FareCalculationModel;
use Taxibeat\Helpers\FareCalculationModelLogger;
use Taxibeat\Config\Config;

if (isset($argv[1])) {
    $inputFileName = $argv[1];

    try {
        $fareCalculationLogger = new FareCalculationModelLogger();
        $fareCalculation = new FareCalculationModel($inputFileName, new Config());
        $fareCalculation->addObserver($fareCalculationLogger);
        $fareCalculation->loadData();
        $fareCalculation->runDataFiltering();
        $fareCalculation->runFareEstimation();
        $fareCalculation->runExportFareEstimateData();
    } catch(\Exception $e) {
        echo $e->getMessage() . "\n";
    }
} else {
    echo "Please, provide input file name from /data/input directory as an argument to this script\n";
}
